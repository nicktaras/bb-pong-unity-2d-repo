﻿#pragma strict

var moveUP : KeyCode;
var moveDown : KeyCode;
var speed : float = 10;

function Update (){
	
	rigidbody2D.velocity.y += speed;

	if(Input.GetKey(moveUP)){
	
		rigidbody2D.velocity.y = speed;
	
	} else if(Input.GetKey(moveDown)){
	
		rigidbody2D.velocity.y = speed * - 1;
	
	} else {
	
		rigidbody2D.velocity.y = 0;
	
	}
	
	rigidbody2D.velocity.x = 0;

}
﻿
#pragma strict

var Ball : Transform;
var speed : float;
var dir : String;
var enemyCanMove = false;
var prevBallPosX : float; // used to save the position of the balls x position //
var foundBall = false;

function Start(){
	
	//start looking to chase the ball
	InvokeRepeating("chaseBall", 0, 1);
	
}

//Get the enemy to look for the y position of the ball, only
//when its heading towards itself//
function chaseBall(){
	
	var oldBallX : float = prevBallPosX;
	var currBallX : float = Ball.position.x;

	if(currBallX > oldBallX){
		enemyCanMove = true;
	} else {
		enemyCanMove = false;
		dir = "none";
	}

}

function Update () {

	if(enemyCanMove){
	
		//this.gameObject.transform.position.y = Ball.position.y;
	
		//if the ball is higher, move the enemy up
		if(Ball.position.y > this.gameObject.transform.position.y / 1.8){
		
			//dir = "up";
			rigidbody2D.velocity.y = speed;
			
		//if the ball is higher, move the enemy down
		} else if(Ball.position.y < this.gameObject.transform.position.y/ 1.8){
		
			//dir = "down";
			rigidbody2D.velocity.y = speed * -1;
		
		} else { // //if the ball the same height dont move
			
			//dir = "none";
			this.gameObject.transform.position.y = Ball.position.y;
		
		}
		
		//if(dir == "up"){
		///} else if(dir == "down"){
			//rigidbody2D.velocity.y = speed * -1;
		//} else {
			//this.gameObject.transform.position.y = Ball.position.y;
		//}
	
	}
			
	rigidbody2D.velocity.x = 0;
	prevBallPosX = Ball.position.x;
		
}

